### Description
This module adds uuid support to ctool contexts. All entity contexts that
has a uuid property are now saved with their uuid instead of their entity_id
respectively uid.

