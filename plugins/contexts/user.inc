<?php

/**
 * Modifying the entity context in hook_ctools_entity_context_alter()
 * for user entity only works when adding new user context. When editing
 * or using the user context (e.g. rendering a panel) old callbacks 
 * ctools_context_create_user() and ctools_context_user_settings_form()
 * are used. So for user context (perhaps for others too) this workaround
 * is required. 
 */
 
$plugins = ctools_get_contexts();
$plugin = $plugins['user'];

$plugin['context original'] = $plugin['context'];
$plugin['edit form original'] = $plugin['edit form'];

$plugin['context'] = 'uuid_ctools_contexts_ctools_context_create_entity';
$plugin['edit form'] = 'uuid_ctools_contexts_ctools_context_entity_settings_form';
